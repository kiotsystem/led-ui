#!/bin/sh

set -e

set_nginx_config() {
    echo 'Will set Nginx configuration'

    envsubst '\$PORT \$WWW_DIR \$HOSTNAME' \
      < /app/nginx-template.conf > \
      /etc/nginx/conf.d/default.conf

    echo 'Set Nginx configuration'
}

if [ "$1" = 'serve' ]; then
    echo "`date` Will start config"
    set_nginx_config
    echo "`date` Run nginx:"
    exec nginx-debug -g 'daemon off;'
fi

exec "${@}"
