### node ###
FROM kiotsystem/vue-cli-arm:latest as builder

WORKDIR /app

COPY ./babel.config.js .
COPY ./package.json .
COPY ./package-lock.json .
COPY ./.env .
COPY ./src/ ./src/
COPY ./public/ ./public/

RUN npm install

RUN npm run build

EXPOSE 8080

# vue cli
EXPOSE 8000

ENTRYPOINT ["/usr/local/bin/npm"]

CMD ["run", "serve"]


### NGINX ###

FROM nginx:1.14 as server

ENV PORT "80"

ENV WWW_DIR "/app/www"

WORKDIR /app

RUN apt-get update && \
    apt-get install -yqq --no-install-recommends gettext && \
    rm -rf /var/lib/apt/lists/*

COPY --from=builder /app/dist/ /app/www/

COPY ./conf/nginx-template.conf .

COPY ./docker-entrypoint.sh .

EXPOSE 80

ENTRYPOINT ["/bin/sh", "/app/docker-entrypoint.sh"]

CMD ["serve"]
